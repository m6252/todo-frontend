const { Pact } = require('@pact-foundation/pact');
const chai = require('chai');

const expect = chai.expect;

const {todoItems, postedData} = require("../tests/unit/mockdata")
const {getTodoItems , postTodoItem}= require("../src/api")

const testUrl = 'http://localhost:4001';

describe('Consumer Tests quiz app API', () => {
	const provider = new Pact({
		port: 4001,
		consumer: 'todo-app-frontend',
		provider: 'todo-app-backend',
		pactfileWriteMode: 'update',
	});

	before(() => provider.setup());

	describe('When a request is sent to Todo API', () => {
		describe('When a GET request is sent to Todo API', () => {
			before(() => {
				return provider.addInteraction({
					uponReceiving: 'All todo items',
					withRequest: {
						path: '/',
						method: 'GET',
					},
					willRespondWith: {
						status: 200,
						body: todoItems,
					},
				});
			});
			it('Will receive the list of todos', async () => {
				await getTodoItems(testUrl).then((r) => {
					expect(r.status).to.be.equal(200);
					expect(r.data).to.deep.equal(todoItems);
				});
			});
		});

		describe('When a POST request is sent to Todo API', () => {
			before(() => {
				return provider.addInteraction({
					uponReceiving: 'Post a new todo',
					withRequest: {
						path: '/',
						method: 'POST',
						body: postedData,
					},
					willRespondWith: {
						status: 201,
					},
				});
			});

			it('Will recive the status code 201', async () => {
				postTodoItem(testUrl, postedData).then((r) => {
					expect(r.status).to.be.equal(201);
				});
			});
		});
	});
	after(() => provider.finalize());
});
