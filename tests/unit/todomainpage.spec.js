import { shallowMount } from '@vue/test-utils'
import Todo from '@/components/Todo.vue'
const {todoItems}= require("./mockdata")

describe('Todo.vue', () => {
  const msg = 'new message'
  const wrapper = shallowMount(Todo, {
    propsData: { msg },
    data() {
      return{
        todos:todoItems.todos,
       
      }
    },
  })
  it('renders props.msg when passed', () => {
   
    expect(wrapper.text()).toMatch(msg)
  })
  it("Is textbox rendered", ()=>{
    expect(wrapper.find('#todo-textbox').exists()).toBeTruthy();
  })
  it("Is add button rendered", ()=>{
    expect(wrapper.find('#todo-addButton').exists()).toBeTruthy();
  })
  it("Is list rendered", ()=>{
    expect(wrapper.find('#todo-list').exists()).toBeTruthy();
  })
  it("Is list item rendered",()=>{
    expect(wrapper.find('#todoList-0').exists()).toBeTruthy();
  })
})
