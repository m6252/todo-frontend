import axios from 'axios';
import MockAdapter from 'axios-mock-adapter'
const {todoItems, postedData} = require("./mockdata")
const {getTodoItems , postTodoItem}= require("../../src/api")

const mock = new MockAdapter(axios);
const testUrl = "http://localhost:4001"
// api tests
describe("Todo app api tests",()=>{
    
    afterAll(() => mock.restore());
    beforeEach(() => mock.reset());
    it("Get todo items from dataset",async ()=>{
        mock.onGet(testUrl).reply(200,todoItems);
        const res = await getTodoItems(testUrl)
        expect(res.status).toBe(200)
        expect(res.data).toStrictEqual(todoItems)
    })
    it("Post todo item to dataset",async ()=>{
        mock.onPost(testUrl).reply(201);
        const res = await postTodoItem(testUrl,postedData);
        expect(res.status).toBe(201)
    })

})