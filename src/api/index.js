const axios = require('axios')

const getTodoItems = async (testUrl) =>
	await axios
		.get(testUrl)
		.then((res) => res)
		.catch((err) => {
			throw err;
		});

    

const postTodoItem = async (testUrl, postedData) =>
await axios
    .post(testUrl, postedData)
    .then((res) => res)
    .catch((err) => {
        throw err;
    });

module.exports = {
    getTodoItems,
    postTodoItem
}