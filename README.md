# Todo App Frontend 

## For Visit Todo App;
* [Product](http://35.227.159.225/) for visit Todo App production
* [Test](http://34.78.1.142/) for visit Todo App Test


## Libraries & Frameworks:
* [Vue Cli](https://cli.vuejs.org/guide/) for install vue
* [Pact.io](https://github.com/pact-foundation/pact-js) for the consumer driven contract (CDC) testing
* [axios](https://www.npmjs.com/package/axios) HTTP requests
* [Mocha](https://github.com/mochajs/mocha) for run tests
* [Axios-Mock-Adapter](https://www.npmjs.com/package/axios-mock-adapter) for mock responses
* [Jest](https://www.npmjs.com/package/jest) for tests 

## Setup & Run
- Run `npm install` for package installations.
- Run `npm run test:unit` for unit tests.
- Run `npm run test:cdc` for cdc tests.
- Run `npm run test:publish` for send cdc test to [Pactflow](https://ehilmidag.pactflow.io/).
- Run `npm run serve` for serve




### Customize Vue configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
